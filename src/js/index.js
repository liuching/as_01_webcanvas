//* variables
var ms = false; //* mouse_status
var cur_mode = 'pen'; //* mode_status
var canvas; //* Canvas
var ctx; //* Context of canvas
var mouse_x, mouse_y;
var press_x, press_y;
var tmp = [];
var draw_history = [];
var undo_history = [];
var drawing = false;
var texting = false;
var filling = false;
var reader = new FileReader();
var img = new Image();
var txtSize = '12pt';
var txtFont = 'Arial';
//* variables

window.addEventListener('DOMContentLoaded', init);

function ur_img() {
	let Uimage = document.getElementById('undo');
	let Rimage = document.getElementById('redo');

	if (draw_history.length) Uimage.style['opacity'] = 1;
	else Uimage.style['opacity'] = 0.1;

	if (undo_history.length) Rimage.style['opacity'] = 1;
	else Rimage.style['opacity'] = 0.1;
}

function up_leave() {
	ms = false;
	ctx.closePath();

	if (tmp.length) draw_history.push(tmp);

	drawing = false;
	tmp = [];

	ur_img();
}

function undo_func() {
	if (!draw_history.length) return;
	undo_history.push(draw_history.pop());
	ctx.clearRect(0, 0, canvas.width, canvas.height);
	restore_data(draw_history);

	ur_img();
}

function update_size() {
	let val = document.getElementById('size_selector').value;
	let text = document.getElementById('size_display');
	text.innerHTML = val + 'px';

	ctx.lineWidth = val;
}

function get_size() {
	return document.getElementById('size_selector').value;
}

function update_color() {
	let val = document.getElementById('color_selector').value;
	let txt = document.getElementById('color_val');
	txt.style.color = val;
	txt.innerHTML = document.getElementById('color_selector').value;

	document.getElementById('color_selector_div').style['border-color'] = val;
	ctx.fillStyle = val;
	ctx.strokeStyle = val;
}

function get_color() {
	return document.getElementById('color_selector').value;
}

function get_font() {
	return txtSize + ' ' + txtFont;
}

function generate_fontBox() {
	let textBox = document.getElementById('textBox');

	if (texting === true) {
		textBox.style['z-index'] = 1;

		let textContent = textBox.value;

		if (textContent !== '') {
			use_text(textContent);
			textBox.value = '';
		}

		texting = false;
	} else {
		textBox.style['left'] = press_x + 'px';
		textBox.style['top'] = press_y + 'px';
		textBox.style['font-family'] = txtFont;
		textBox.style['font-size'] = txtSize;
		textBox.style['color'] = get_color();

		textBox.style['z-index'] = 6;
		texting = true;
	}
}

function reset_display() {
	let _list = ['pen', 'eraser', 'circle', 'rectangle', 'triangle', 'text'];

	for (let i = 0; i < _list.length; i++)
		document.getElementById(_list[i]).style['background-color'] =
			'transparent';
}

function init() {
	canvas = document.getElementById('main_canvas');
	ctx = canvas.getContext('2d');
	ctx.lineWidth = 5;

	canvas.addEventListener('mousedown', evt => {
		if (texting === false) {
			press_x = evt.offsetX;
			press_y = evt.offsetY;
		}

		ms = true;

		if (cur_mode === 'text') generate_fontBox();
		else ctx.beginPath();
	});
	canvas.addEventListener('mouseup', up_leave);
	canvas.addEventListener('mouseleave', up_leave);
	canvas.addEventListener('mousemove', mouse_moving);
	ctx.lineCap = 'round';
	document.getElementById('undo').addEventListener('click', undo_func);
	document.getElementById('redo').addEventListener('click', () => {
		if (!undo_history.length) return;

		let data = undo_history.pop();
		let out = [];
		out.push(data);

		restore_data(out);
		draw_history.push(data);

		ur_img();
	});
	document.getElementById('pen').addEventListener('click', () => {
		cur_mode = 'pen';
		reset_display();
		document.getElementById('pen').style['background-color'] =
			'rgba(237, 182, 17, 1)';
		canvas.style['cursor'] = "url('src/img/cursor/001-pen.ico'), auto";
	});
	document.getElementById('eraser').addEventListener('click', () => {
		cur_mode = 'eraser';
		reset_display();
		document.getElementById('eraser').style['background-color'] =
			'rgba(237, 182, 17, 1)';
		console.log('hi');
		canvas.style['cursor'] = 'url("src/img/cursor/008-eraser.ico"), auto';
	});
	document.getElementById('text').addEventListener('click', () => {
		cur_mode = 'text';
		reset_display();
		document.getElementById('text').style['background-color'] =
			'rgba(237, 182, 17, 1)';
		canvas.style['cursor'] = "url('src/img/cursor/031-letters.ico'), auto";
	});
	document.getElementById('circle').addEventListener('click', () => {
		cur_mode = 'circle';
		reset_display();
		document.getElementById('circle').style['background-color'] =
			'rgba(237, 182, 17, 1)';
		canvas.style['cursor'] = "url('src/img/cursor/circle.ico'), auto";
	});
	document.getElementById('rectangle').addEventListener('click', () => {
		cur_mode = 'rectangle';
		reset_display();
		document.getElementById('rectangle').style['background-color'] =
			'rgba(237, 182, 17, 1)';
		canvas.style['cursor'] = "url('src/img/cursor/rect.ico'), auto";
	});
	document.getElementById('triangle').addEventListener('click', () => {
		cur_mode = 'triangle';
		reset_display();
		document.getElementById('triangle').style['background-color'] =
			'rgba(237, 182, 17, 1)';
		canvas.style['cursor'] = "url('src/img/cursor/triangle.ico'), auto";
	});
	document.getElementById('fill').addEventListener('click', () => {
		filling = !filling;
		
		let val = (filling ? 'rgba(227, 45, 0, 0.8)' : 'transparent');
		document.getElementById('fill').style['background-color'] = val;
	});
	document.getElementById('download').addEventListener('click', () => {
		var img_link = document.createElement('a');

		img_link.download = 'download.png';
		img_link.href = canvas.toDataURL();

		img_link.click();
		img_link.delete;
	});

	document.getElementById('upload').addEventListener('change', evt => {
		reader.readAsDataURL(evt.target.files[0]);
	});
	reader.addEventListener('load', () => {
		img.src = reader.result;
	});
	img.addEventListener('load', () => {
		canvas.width = img.width;
		canvas.height = img.height;
		ctx.drawImage(img, 0, 0);

		ctx.lineCap = 'round';
		update_color();
		update_size();

		store_data('image', img);
	});

	document.getElementById('text_size').addEventListener('change', () => {
		txtSize = document.getElementById('text_size').value + 'pt';
	});

	document.getElementById('text_font').addEventListener('change', () => {
		txtFont = document.getElementById('text_font').value;
	});
	
	document.getElementById('linecap_selector').addEventListener('change', () => {
		ctx.lineCap = document.getElementById('linecap_selector').value;
	});

	document.getElementById('reset').addEventListener('click', () => {
		let val = confirm('Are you sure about that?');

		if (val) {
			canvas.width = 800;
			canvas.height = 800;

			while (draw_history.length > 0) undo_func();

			draw_history = [];
			undo_history = [];
		}
	});

	document
		.getElementById('size_selector')
		.addEventListener('change', update_size);

	document
		.getElementById('color_selector')
		.addEventListener('change', update_color);

	draw_history = [];
	undo_history = [];
	// FIXME:
	// draw();
}

function mouse_moving(evt) {
	if (ms === false) {
		[mouse_x, mouse_y] = get_pos(evt);
		return;
	}

	undo_history = [];
	switch (cur_mode) {
		case 'pen':
			use_pen(evt);
			break;
		case 'eraser':
			use_eraser(evt);
			break;
		case 'circle':
			use_circle(evt);
			break;
		case 'rectangle':
			use_rect(evt);
			break;
		case 'triangle':
			use_triangle(evt);
			break;
		case 'upload':
			use_upload(evt);
			break;
	}

	[mouse_x, mouse_y] = get_pos(evt);
}

function store_data(...arg) {
	tmp.push(arg);
}

function force_save() {
	if (tmp.length) draw_history.push(tmp);
	tmp = [];
}

function restore_data(src) {
	for (let i = 0; i < src.length; i++) {
		ctx.beginPath();
		for (let k = 0; k < src[i].length; k++) {
			switch (src[i][k][0]) {
				case 'eraser':
					ctx.globalCompositeOperation = 'destination-out';
				case 'pen':
					// color.
					ctx.strokeStyle = src[i][k][5];
					ctx.fillStyle = src[i][k][5];
					// size.
					ctx.lineWidth = src[i][k][6];

					ctx.moveTo(src[i][k][1], src[i][k][2]);
					ctx.lineTo(src[i][k][3], src[i][k][4]);
					ctx.stroke();

					ctx.globalCompositeOperation = 'source-over';
					break;
				case 'circle':
					// color.
					ctx.strokeStyle = src[i][k][5];
					ctx.fillStyle = src[i][k][5];
					// size.
					ctx.lineWidth = src[i][k][6];

					ctx.arc(
						src[i][0][1],
						src[i][0][2],
						src[i][0][3],
						0,
						2 * Math.PI,
						true
					);

					if (src[i][0][4]) ctx.fill();
					else ctx.stroke();

					break;
				case 'rectangle':
					// color.
					ctx.strokeStyle = src[i][k][6];
					ctx.fillStyle = src[i][k][6];
					// size.
					ctx.lineWidth = src[i][k][7];

					if (src[i][k][5])
						ctx.fillRect(
							src[i][k][1],
							src[i][k][2],
							src[i][k][3],
							src[i][k][4]
						);
					else
						ctx.strokeRect(
							src[i][k][1],
							src[i][k][2],
							src[i][k][3],
							src[i][k][4]
						);

					break;
				case 'triangle':
					// color.
					ctx.strokeStyle = src[i][k][6];
					ctx.fillStyle = src[i][k][6];

					// size.
					ctx.lineWidth = src[i][k][7];
						
					if (src[i][k][5])
					{
						ctx.moveTo(src[i][k][3], src[i][k][4]);
						ctx.lineTo(src[i][k][1], src[i][k][4]);
						ctx.lineTo((src[i][k][3] + src[i][k][1]) / 2, src[i][k][2]);
						ctx.fill();
					}
					else
					{
						ctx.moveTo(src[i][k][3], src[i][k][4]);
						ctx.lineTo(src[i][k][1], src[i][k][4]);
						ctx.moveTo(src[i][k][1], src[i][k][4]);
						ctx.lineTo((src[i][k][3] + src[i][k][1]) / 2, src[i][k][2]);
						ctx.moveTo((src[i][k][3] + src[i][k][1]) / 2, src[i][k][2]);
						ctx.lineTo(src[i][k][3], src[i][k][4]);
						ctx.stroke();
					}

					break;
				case 'image':
					canvas.width = src[i][k][1].width;
					canvas.height = src[i][k][1].height;
					ctx.drawImage(src[i][k][1], 0, 0);
					ctx.lineCap = 'round';
					break;
				case 'text':
					ctx.strokeStyle = src[i][k][5];
					ctx.fillStyle = src[i][k][5];
					ctx.font = src[i][k][4];

					ctx.fillText(src[i][k][1], src[i][k][2], src[i][k][3]);
					break;
			}

			update_color();
			update_size();
		}
		ctx.closePath();
	}
}

function use_pen(evt) {
	ctx.globalCompositeOperation = 'source-over';
	let pos = get_pos(evt);

	ctx.moveTo(mouse_x, mouse_y);
	ctx.lineTo(pos[0], pos[1]);
	ctx.stroke();

	store_data(
		'pen',
		mouse_x,
		mouse_y,
		pos[0],
		pos[1],
		get_color(),
		get_size()
	);
}

function use_text(str) {
	let textX = press_x + 2;
	let textY = press_y + parseInt(txtSize.slice(0, -2)) + 5;

	ctx.font = txtSize + ' ' + txtFont;

	ctx.beginPath();
	ctx.fillText(str, textX, textY);
	ctx.closePath();

	store_data('text', str, textX, textY, get_font(), get_color());
}

function use_eraser(evt) {
	ctx.globalCompositeOperation = 'destination-out';
	let pos = get_pos(evt);

	ctx.moveTo(mouse_x, mouse_y);
	ctx.lineTo(pos[0], pos[1]);
	ctx.stroke();
	ctx.globalCompositeOperation = 'source-over';

	store_data(
		'eraser',
		mouse_x,
		mouse_y,
		pos[0],
		pos[1],
		get_color(),
		get_size()
	);
}

function use_circle(evt) {
	if (drawing) {
		ctx.closePath();
		undo_func();
		undo_history = [];
		ctx.beginPath();
	}

	let radius =
		Math.min(Math.abs(press_x - mouse_x), Math.abs(press_y - mouse_y)) / 2;

	let cir_x = mouse_x > press_x ? press_x + radius : press_x - radius;
	let cir_y = mouse_y > press_y ? press_y + radius : press_y - radius;

	ctx.arc(cir_x, cir_y, radius, 0, 2 * Math.PI, true);

	if (filling) ctx.fill();
	else ctx.stroke();

	store_data(
		'circle',
		cir_x,
		cir_y,
		radius,
		filling,
		get_color(),
		get_size()
	);
	force_save();

	drawing = true;
}

function use_rect(evt) {
	if (drawing) {
		ctx.closePath();
		undo_func();
		undo_history = [];
		ctx.beginPath();
	}

	if (filling)
		ctx.fillRect(press_x, press_y, mouse_x - press_x, mouse_y - press_y);
	else
		ctx.strokeRect(press_x, press_y, mouse_x - press_x, mouse_y - press_y);

	store_data(
		'rectangle',
		press_x,
		press_y,
		mouse_x - press_x,
		mouse_y - press_y,
		filling,
		get_color(),
		get_size()
	);
	force_save();

	drawing = true;
}

function use_triangle(evt) {
	if (drawing) {
		ctx.closePath();
		undo_func();
		undo_history = [];
		ctx.beginPath();
	}

	if (filling) {
		ctx.moveTo(mouse_x, mouse_y);
		ctx.lineTo(press_x, mouse_y);
		ctx.lineTo((mouse_x + press_x) / 2, press_y);
		ctx.fill();
	} else {
		ctx.moveTo(mouse_x, mouse_y);
		ctx.lineTo(press_x, mouse_y);
		ctx.moveTo(press_x, mouse_y);
		ctx.lineTo((mouse_x + press_x) / 2, press_y);
		ctx.moveTo((mouse_x + press_x) / 2, press_y);
		ctx.lineTo(mouse_x, mouse_y);
		ctx.stroke();
	}

	store_data(
		'triangle',
		press_x,
		press_y,
		mouse_x,
		mouse_y,
		filling,
		get_color(),
		get_size()
	);
	force_save();

	drawing = true;
}

function get_pos(evt) {
	return [evt.offsetX, evt.offsetY];
}

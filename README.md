# Software Studio 2021 Spring
## Assignment 01 Web Canvas

### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Action tools highlighting                        | 1~5%      | Y         |
| Undo, Redo hided when unavailable                | 1~5%      | Y         |
| Color code display                               | 1~5%      | Y         |
| Text preview when typing                         | 1~5%      | Y         |
| Changing LineCap                                 | 1~5%      | Y         |
| Switch fill and stroke button                    | 1~5%      | Y         |


---

### How to use 

* **第一排：繪圖工具**
    * 筆刷（可調整顏色、大小、Linecap）
    * 橡皮擦（可調整大小、Linecap）
    * 圖形工具（可調整顏色、大小）
    * 填滿工具（切換圖形fill or stroke）
* **第二排：文字工具**
    * 文字工具（可調整字型、顏色、大小）
* **第三排：通用工具**
    * Undo（可用時回復正常透明度）
    * Redo（可用時回復正常透明度）
    * Reset
* **第四排：上傳下載工具**
    * Download
    * Upload
    

### Function description

    1. Click "Pen" to draw as you like.
    2. If you find something wrong, you can click "Eraser" to erase things makes you crazy on the canvas. (It can't erase shitty things in your life, I'm sorry.)
    3. Or you can click "Undo" to make those stains disappear and the canvas will return to the canvas before you fucked up.
    4. If you click too many times of "Undo", you can still click "Redo" to have your adorable paintings back.
    5. If the canvas becomes as horrible as your life, click "Refresh" and you will get a brand new canvas. (It can't refresh your life, too, I'm sorry, again.)
    
    -referred to 108062120

##### Variables

`cur_mode`: 目前使用的工具

`draw_history`: Undo history

`undo_history`: Redo history

`tmp`: ***紀錄一筆劃中所有的資料（見Undo, Redo Function）***

`mouse_x, mouse_y`: 滑鼠座標

`press_x, press_y`: 滑鼠開始筆劃的座標

##### Functions

* Basic Function
    * `init`: 當網頁架構載入完畢時觸發(Line 19)，初始化所有EventListener，
    * `get_xxx`: 獲取當前資料（size, color, font）
    * `use_xxx`: 使用繪圖工具（pen, eraser, rect, circle, triangle, text）
    * `mouse_moving`: 當滑鼠移動時觸發（Line 139），偵測滑鼠是否處於按下狀態（variable ms），再決定要執行什麼動作。
    * `generate_fontBox`: 文字工具時處理fontarea的函數。
* Undo, Redo
    * **variable tmp**: 紀錄自滑鼠按下開始到放開，所有過程中所繪製出的線段Data。
    * `store_data`: 將目前的tmp推進draw_history中並清空tmp。
    * `restore_data`: 重新依照draw_history繪製一次儲存的資料，用以實作Undo。
    * `force_save`: 強制push tmp into draw_history，在draw rect, circle, triangle時會用到。
    * `undo_func`: 將top of draw_history提出存入undo_history，並清空畫布並呼叫restore_data。
* CSS Function
    * `reset_display`: 重設所有按鈕的appearance
    * `ur_img`: 重設undo, redo button的appearance。

##### Undo實現方式
###### Reference
[探究canvas繪圖中撤銷（undo）功能的實現方式](https://codertw.com/程式語言/718668/)

我參考上述網站的內容，認為截圖復原正如他所說的，效能上可能會有低落的問題，因此採用下面的方式，即將所有步驟的資料都記錄下來，再清空畫布重畫一次。

使用者使用pen畫出的筆畫非常大的機率不是直線，因此只能利用許多直線構造出曲線。我們將這一筆劃（很多直線）儲存在tmp裡，當滑鼠放開時push進array裡，undo時再以tmp為單位復原資料。

##### Advanced Function
1. 使用中的工具會亮起（CSS）。
2. 當可以使用(undo || redo)時，才顯示(undo || redo)按鈕，否則降低透明度。
3. 顯示選擇color的color code。
4. 改變textarea的CSS，讓其可以在編輯文字時就預覽結果。
5. 改變LineCap。
6. 當油漆桶高亮時，圖形將以填滿呈現。

### Gitlab page link

[https://108062330.gitlab.io/AS_01_WebCanvas](https://108062330.gitlab.io/AS_01_WebCanvas)

### Others (Optional)

**評分...辛苦...了...**

![](https://i.imgur.com/zgUCX6A.png)

<style>
table th{
    width: 100%;
}
</style>
